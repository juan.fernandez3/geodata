from django.urls import include, path
from rest_framework import routers
from api import views

# URL's for create and get dataset, get and filter rows
urlpatterns = [
    path('datasets/', views.dataset_list),
    path('rows/', views.row_list)
]
