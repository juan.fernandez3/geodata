from api.models import Row, DataSet
from rest_framework.serializers import ModelSerializer

# Serializer class for Row
class RowSerializer(ModelSerializer):
    class Meta:
        model = Row
        fields = ('id', 'point', 'client_id', 'client_name')


# Serializer class for DataSet
class DataSetSerializer(ModelSerializer):

    class Meta:
        model = DataSet
        fields = ('id', 'name', 'date')