from django.contrib.gis.db import models

# Model for DataSet description in database
class DataSet(models.Model):
    name = models.CharField(max_length=95)
    date = models.DateField(auto_now_add=True)

    @classmethod
    def create(cls, name):
        dataset = cls(name=name)
        dataset.save()
        return dataset

    def __str__(self):
        return self.name
        

# Model for Row description in database
class Row(models.Model):
    point = models.PointField()
    client_id = models.IntegerField()
    client_name = models.CharField(max_length=45)
    dataset = models.ForeignKey(DataSet, on_delete=models.CASCADE)

    @classmethod
    def create(cls, point, client_id, client_name, dataset):
        row = cls(point=point,client_id=client_id, client_name=client_name, dataset=dataset)
        row.save()
        return row

