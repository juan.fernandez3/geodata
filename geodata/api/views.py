from api.models import Row, DataSet
from rest_framework.decorators import api_view
from rest_framework.response import Response
from api.serializers import DataSetSerializer, RowSerializer
from rest_framework import status
import csv
import codecs
from django.contrib.gis.geos import Point
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


# GET: Get dataset list from db with paginate service
# POST: Insert dataset and rows from csv file getting from form_data
@api_view(['GET', 'POST'])
def dataset_list(request):

    if request.method == 'GET':
        dataset_list = DataSet.objects.all().order_by('id')
        page = int(request.GET.get('page',1))
        paginator = Paginator(dataset_list, 5)
        number_pages = paginator.num_pages

        if page > 0 and page <= number_pages:
            datasets = paginator.page(page)
            serializer = DataSetSerializer(datasets, many=True)
            return Response(serializer.data)
        else:
            return Response({'message': 'number pages are {0}'.format(number_pages)}, status=status.HTTP_400_BAD_REQUEST)

    if request.method == 'POST':
        if 'name' in request.data and 'file' in request.FILES:
            name = request.data['name']
            csv_file = request.FILES['file']
            serializer = DataSetSerializer(data={'name':name})

            if csv_file.name.endswith('.csv'):
                stream = codecs.iterdecode(csv_file, 'utf-8')
                csv_reader = csv.reader(stream, delimiter=',')

                if serializer.is_valid():
                    dataset = serializer.save()
                    stream = codecs.iterdecode(csv_file, 'utf-8')
                    csv_reader = csv.reader(stream, delimiter=',')
                    line_count = 0
                    for row in csv_reader:
                        if line_count != 0:
                            point = Point(float(row[0]), float(row[1]))
                            client_id = row[2]
                            clien_name = row[3]
                            Row.create(point, client_id, clien_name, dataset)   
                        line_count += 1

                    return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response({'message': 'file extension is not .csv'}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'message': 'name or file param not found'}, status=status.HTTP_400_BAD_REQUEST)


# GET: Get rows from database filtering by dataset_id, client_name or point(lat, lng)
# Query parms must be sended like /api/v1/rows/?dataset_id=5 for example
@api_view(['GET'])
def row_list(request):

    if request.method == "GET":
        row_list = Row.objects.all()
        dataset_id = request.query_params.get('dataset_id', None)
        name = request.query_params.get('name', None)
        lat = request.query_params.get('lat', None)
        lng = request.query_params.get('lng', None)

        if dataset_id is not None:
            row_list = row_list.filter(dataset__id=dataset_id)

        if name is not None:
            row_list = row_list.filter(client_name__iexact=name.lower())

        if lat is not None and lng is not None:
            try:
                point = Point(float(lat), float(lng))
                row_list = row_list.filter(point=point)
            except:
                return Response({'message': 'point format is not valid'}, status=status.HTTP_400_BAD_REQUEST)

        serializer = RowSerializer(row_list, many=True)
        return Response(serializer.data)


