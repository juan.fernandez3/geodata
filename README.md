 GeoData for Abraxas
 
 Django project with API REST for insert and get georefenced poinst from a datasets.

## Getting Started

1) Clone project from https://gitlab.com/juan.fernandez3/geodata
2) Open a terminal and install prerequisites
3) Change .env.example for .env and change user and db name

### Prerequisites

1) Python 3.7+
2) Docker
3) Docker Compose

## Deployment 

1) Open a terminal into the project directory
2) build docker image with sudo docker-compose build
3) execute docker-compose up

## EndPoints 

1) localhost:8000/api/v1/datasets/
	- POST: send file(csv) and dataset name (name) in form_data
	- GET: return 5 register, you can specify page number like:
		- localhost:8000/api/v1/datasets/?page=1
6) localhost:8000/api/v1/rows/ 
	- GET: you can add parameters for filtering like:
		- localhost:8000/api/v1/rows/?dataset_id=5
		- localhost:8000/api/v1/rows/?name=test
		- localhost:8000/api/v1/rows/?lat=25.25&lng=25.25
		
## Built With

* [Django](https://www.djangoproject.com/) - The web framework used
* [Pip](https://pypi.org/project/pip/) - Package Management
* [Postgres](https://www.postgresql.org/) - Database used
* [Docker](https://www.docker.com/) - Container used

## Contributing

Please read (https://gitlab.com/juan.fernandez3/geodata) for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* **Juan David Fernandez Diaz**

## License

This project is licensed under the MIT License

